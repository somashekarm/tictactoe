﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable] public class Player
{
    public Image panel;
    public Text text;
    public Button button;
}

[System.Serializable] public class PlayerColor
{
    public Color panelColor;
    public Color textColor;
}

public class GameController : MonoBehaviour
{
    public Text[] buttonList;
    private string playerSide;
    public GameObject gameOverPanel;
    public Text gameOverText;
    private int moveCount;
    public GameObject restartGameButton;
    public Player playerX; 
    public Player playerO; 
    public PlayerColor activePlayerColor; 
    public PlayerColor inactivePlayerColor;
    public GameObject startInfo;
    public AudioSource buttonClickedAudio;
    public GameObject QuitButton;
    public GameObject MuteButton;
    public GameObject UnMuteButton;




    private void Awake()
    {
        SetGameControllerReferenceOnButtons();
        //playerSide = "X";
        //SetPlayerColors(playerX, playerO);
        gameOverPanel.SetActive(false);
        moveCount = 0;
        restartGameButton.SetActive(false);
        QuitButton.SetActive(false);
        UnMuteButton.SetActive(false);
        MuteButton.SetActive(false);

    }


    void SetGameControllerReferenceOnButtons()
    {
        for(int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<GridSpace>().SetGameControllerReference(this);
        }
    }

  
    public string GetPlayerSide()
    {
        return playerSide;
    }

    void SetPlayerColors(Player newPlayer, Player oldPlayer)
    {
        newPlayer.panel.color = activePlayerColor.panelColor; 
        newPlayer.text.color = activePlayerColor.textColor;
        oldPlayer.panel.color = inactivePlayerColor.panelColor; 
        oldPlayer.text.color = inactivePlayerColor.textColor;
    }

    void SetPlayerColorsInactive()
    {
        playerX.panel.color = inactivePlayerColor.panelColor;
        playerX.text.color = inactivePlayerColor.textColor;
        playerO.panel.color = inactivePlayerColor.panelColor;
        playerX.text.color = inactivePlayerColor.textColor;
    }

    public void SetStartingSide(string startingSide)
    {
        playerSide = startingSide;

        if(playerSide == "X")
        {
            SetPlayerColors(playerX, playerO);
        }
        else
        {
            SetPlayerColors(playerO, playerX);
        }
        StartGame();

        
    }

    void WinnerPatternDisplay(int a, int b, int c)
    {
        buttonList[a].color = Color.red;
        buttonList[b].color = Color.red;
        buttonList[c].color = Color.red;
    }

    public void PlayerMoveSound()
    {
        buttonClickedAudio.Play();
    }

    public void mute()
    {
         AudioListener.pause = true;
         UnMuteButton.SetActive(true);
         MuteButton.SetActive(false);

    }

    public void unMute()
    {
        AudioListener.pause = false;
        UnMuteButton.SetActive(false);
        MuteButton.SetActive(true);

    }

 


    public void GameQuit()
    {
        Application.Quit();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameQuit();
        }
    }

    public void EndTurn()
    {
        moveCount ++;
        if(buttonList[0].text == playerSide && buttonList[1].text == playerSide && buttonList[2].text == playerSide)
        {
            WinnerPatternDisplay(0,1,2);
            GameOver(playerSide);
        }
        else if (buttonList[3].text == playerSide && buttonList[4].text == playerSide && buttonList[5].text == playerSide)
        {
            WinnerPatternDisplay(3,4,5);
            GameOver(playerSide);
        }
        else if(buttonList[6].text == playerSide && buttonList[7].text == playerSide && buttonList[8].text == playerSide)
        {
            WinnerPatternDisplay(6,7,8);
            GameOver(playerSide);
        }
        else if(buttonList[0].text == playerSide && buttonList[3].text == playerSide && buttonList[6].text == playerSide)
        {
            WinnerPatternDisplay(0, 3, 6);
            GameOver(playerSide);
        }
        else if (buttonList[1].text == playerSide && buttonList[4].text == playerSide && buttonList[7].text == playerSide)
        {
            WinnerPatternDisplay(1, 4, 7);
            GameOver(playerSide);
        }
        else if(buttonList[2].text == playerSide && buttonList[5].text == playerSide && buttonList[8].text == playerSide)
        {
            WinnerPatternDisplay(2, 5, 8);
            GameOver(playerSide);
        }
        else if(buttonList[0].text == playerSide && buttonList[4].text == playerSide && buttonList[8].text == playerSide)
        {
            WinnerPatternDisplay(0, 4, 8);
            GameOver(playerSide);
        }
        else if(buttonList[2].text == playerSide && buttonList[4].text == playerSide && buttonList[6].text == playerSide)
        {
            WinnerPatternDisplay(2, 4, 6);
            GameOver(playerSide);
        }
        else if(moveCount >= 9)
        {
            GameOver("draw");
        }
        else
        {
            ChangeSides();
        }
        
  
    }

    void SetBoardInteractable(bool toggle)
    {
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<Button>().interactable = toggle;

        }
    }

    void SetPlayerButtons(bool toggle)
    {
        playerX.button.interactable = toggle;
        playerO.button.interactable = toggle;
    }

    void StartGame()
    {   // removed in RestartGame()
        SetBoardInteractable(true);
        SetPlayerButtons(false);
        startInfo.SetActive(false);
        AudioListener.pause = false;
        QuitButton.SetActive(false);
        MuteButton.SetActive(true);

    }
    public void RestartGame()
    {
        //playerSide = "X";
        moveCount = 0;
        gameOverPanel.SetActive(false);
        restartGameButton.SetActive(false);
        SetPlayerButtons(true);
        //SetPlayerColors(playerX, playerO);
        SetPlayerColorsInactive();
        QuitButton.SetActive(false);
        startInfo.SetActive(true);
        //SetBoardInteractable(true);
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].text = "";
            // Resetting   WinnerPatternDisplay button text color
            buttonList[i].color = Color.white;

        }
    }

    void SetGameOverText(string value)
    {
        gameOverPanel.SetActive(true);
        gameOverText.text =  value;
    }

    void GameOver(string winningPlayer)
    {
        if(winningPlayer == "draw")
        {
            SetGameOverText(" It's a Draw!");
            SetPlayerColorsInactive();
        }
        else
        {
            SetGameOverText(playerSide + " Wins!");
        }
         
        SetBoardInteractable(false);
        restartGameButton.SetActive(true);
        QuitButton.SetActive(true);
        UnMuteButton.SetActive(false);
        MuteButton.SetActive(false);
    }

    void ChangeSides()
    {
        playerSide = (playerSide == "X") ? "O" : "X";
        if (playerSide == "X")
        {
            SetPlayerColors(playerX, playerO);
        }
        else
        {
            SetPlayerColors(playerO, playerX);
        }
    }
}
