﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonRename : MonoBehaviour
{
 public void SetText(string text)
    {
        Text myText = transform.Find("Text").GetComponent<Text>();
        myText.text = text;

    }

}
