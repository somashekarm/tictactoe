# Tic-Tac-Toe
Built a Tic-Tac-Toe Game Using Only **Unity UI Components**. [TicTacToeV1 Android apk](/Build/) is in the **Build folder**(contains other versions according to tutorial steps). Copy and store it on your **android device,** then install it if you want to play with it

### Requirements:
1. [Unity](https://unity.com/products)
2. C# 
3. MS Visual Studio
4. Android Device
5. Windows

### Output:
![Output Image](/Assets/Tic-Tac-Toe.gif)

### Source:
[Tic-Tac-Toe Game](https://learn.unity.com/tutorial/creating-a-tic-tac-toe-game-using-only-ui-components#)